<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{PostController,CommentController};
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('posts', PostController::class);

Route::resource('comments', CommentController::class);

Route::get('/comments-reply', [CommentController::class, 'commentReply']);//all reply against each comment

// Route::post('/comments', [CommentController::class, 'store'])->name('comments.store');

// Route::get('/comments', [CommentController::class, 'index'])->name('comments.show');

// Route::put('/comments/{id}', [CommentController::class, 'update']);


