<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::withcount(['comments'])->get();
        return response()->json([
            "success" => true,
            "message" => "Post List",
            "data" => $posts
        ]);
    }
    /**
     * Write Your Code..
     *
     * @return string
     */
    /**
     * Write Your Code..
     *
     * @return string
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $post = Post::create($input);
        return response()->json([
            "success" => true,
            "message" => "Post created successfully.",
            "data" => $post
        ]);
    }
    /**
     * Write Your Code..
     *
     * @return string
     */
    public function show($id)
    {
        $post = Post::find($id);
        return response()->json([
            "success" => true,
            "message" => "Post retrieved successfully.",
            "data" => $post
        ]);
    }

    public function update(Request $request, Post $post)
    {
        $input = $request->all();
        $request->validate([
            'title' => 'required',
            'body' => 'required',
        ]);
        $post->title = $input['title'];
        $post->body = $input['body'];
        $post->save();
        return response()->json([
            "success" => true,
            "message" => "Post updated successfully.",
            "data" => $post
        ]);
    }
}
