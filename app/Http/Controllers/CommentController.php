<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function index()
    {
        $comment = Comment::withcount(['comments','replies','users'])->with('users')->get();
        return response()->json([
            "success" => true,
            "message" => "Comment List",
            "data" => $comment
        ]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $request->validate([
            'body'=>'required',
        ]);
        // $input['user_id'] = auth()->user()->id;
       $comment=Comment::create($input);
        if($comment->parent_id!=null)
        {
            return response()->json([
                "success" => true,
                "message" => "Reply Created successfully.",
                "data" => $comment
            ]);
        }
        else{
            return response()->json([
                "success" => true,
                "message" => "Comment Created successfully.",
                "data" => $comment
            ]);
        }
        return back();
    }
    
    public function update(Request $request, Comment $comment)
    {
        $input = $request->all();
        $request->validate([
            'body' => 'required',
        ]);
        $comment->post_id=$comment->post_id;
        $comment->user_id=$comment->user_id;
        $comment->body = $input['body'];
        $comment->save();
        if($comment->parent_id!=null)
        {
            return response()->json([
                "success" => true,
                "message" => "Reply updated successfully.",
                "data" => $comment
            ]);
        }
        else{
            return response()->json([
                "success" => true,
                "message" => "Comment updated successfully.",
                "data" => $comment
            ]);
        }
        
    }

    public function commentReply()
    {
        $comment = Comment::with('replies')->get();
        return response()->json([
            "success" => true,
            "message" => "Comment-Reply List",
            "data" => $comment
        ]);
    }

}
